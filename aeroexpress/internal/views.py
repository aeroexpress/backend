import datetime

from django.db.models import Count, Sum
from django.db.models.functions import TruncDate
from django.forms import model_to_dict
from rest_framework import viewsets
from rest_framework.response import Response

from .logic.parse_xlsx import parse_xlsx, get_xlsx_template
from .logic.create_pdf import create_pdf_act, create_pdf_invoice, create_pdf_ticket, create_image_qr
from .logic.send_email import send_email, send_email_confirmation
from .models import Profile, Account, Document, Individual, LegalEntity, Contract, Order, Ticket
from .serializers import ProfileSerializer, AccountSerializer, DocumentSerializer, \
    IndividualSerializer, ShortProfileSerializer, ContractSerializer, LegalEntitySerializer, OrderSerializer

# LOGIC


class UserDataView(viewsets.ViewSet):
    def list(self, request):
        profile = Profile.objects.filter(uuid=request.headers['Authorization']).first()
        if not profile:
            return
        serializer = ShortProfileSerializer(profile, many=False)
        return Response(serializer.data)

    def create(self, request):
        pass

    def retrieve(self, request, pk=None):
        pass

    def update(self, request, pk=None):
        pass

    def partial_update(self, request, pk=None):
        pass

    def destroy(self, request, pk=None):
        pass


class CreateLegalEntityView(viewsets.ViewSet):
    def list(self, request):
        pass

    def create(self, request):
        current_user = Profile.objects.filter(uuid=request.headers['Authorization']).first()
        if not current_user:
            return
        profile = Profile.objects.create(
            first_name=request.data['profile']['first_name'],
            last_name=request.data['profile']['last_name'],
            middle_name=request.data['profile']['middle_name'],
            phone=request.data['profile']['phone'],
            email=request.data['profile']['email'],
            password=request.data['profile']['password'],
            role=request.data['profile']['role'],
        )
        account = Account.objects.create(
            number=request.data['account']['number'],
            correspondent_account=request.data['account']['correspondent_account'],
            bik=request.data['account']['bik'],
            bank=request.data['account']['bank'],
        )
        contract = Contract.objects.create(
            start_date=datetime.datetime.strptime(request.data['contract']['start_date'], "%Y-%m-%d").date(),
            end_date=datetime.datetime.strptime(request.data['contract']['end_date'], "%Y-%m-%d").date(),
            number=request.data['contract']['number'],
            is_credit_limit_enabled=request.data['contract']['is_credit_limit_enabled'],
            credit_amount=request.data['contract']['credit_amount'],
        )
        legal_entity = LegalEntity.objects.create(
            full_name=request.data['legal_entity']['full_name'],
            legal_address=request.data['legal_entity']['legal_address'],
            actual_address=request.data['legal_entity']['actual_address'],
            inn=request.data['legal_entity']['inn'],
            kpp=request.data['legal_entity']['kpp'],
            ogrn=request.data['legal_entity']['ogrn'],
            account=account,
            partner_user=profile,
            contract=contract,
            manager=current_user
        )
        send_email(
            from_address='aeroexpresstest@yandex.ru',
            to=profile.email,
            legal_entity=legal_entity,

        )
        return Response({
            'legal_entity': model_to_dict(legal_entity),
            'profile': model_to_dict(profile),
            'contract': model_to_dict(contract),
            'account': model_to_dict(account),
        })

    def retrieve(self, request, pk=None):
        current_user = Profile.objects.filter(uuid=request.headers['Authorization']).first()
        if not current_user:
            return
        if pk != '0':
            legal_entity = LegalEntity.objects.filter(id=pk).first()
        else:
            legal_entity = LegalEntity.objects.filter(partner_user=current_user).first()
        return Response({
            'legal_entity': model_to_dict(legal_entity),
            'profile': model_to_dict(legal_entity.partner_user),
            'contract': model_to_dict(legal_entity.contract),
            'account': model_to_dict(legal_entity.account),
        })

    def update(self, request, pk=None):
        current_user = Profile.objects.filter(uuid=request.headers['Authorization']).first()
        if not current_user:
            return
        legal_entity = LegalEntity.objects.filter(id=pk).first()

        Profile.objects.filter(id=legal_entity.partner_user.id).update(
            first_name=request.data['profile']['first_name'],
            last_name=request.data['profile']['last_name'],
            middle_name=request.data['profile']['middle_name'],
            phone=request.data['profile']['phone'],
            email=request.data['profile']['email'],
            password=request.data['profile']['password'],
            role=request.data['profile']['role'],
        )
        Account.objects.filter(id=legal_entity.account.id).update(
            number=request.data['account']['number'],
            correspondent_account=request.data['account']['correspondent_account'],
            bik=request.data['account']['bik'],
            bank=request.data['account']['bank'],
        )
        Contract.objects.filter(id=legal_entity.contract.id).update(
            start_date=datetime.datetime.strptime(request.data['contract']['start_date'], "%Y-%m-%d").date(),
            end_date=datetime.datetime.strptime(request.data['contract']['end_date'], "%Y-%m-%d").date(),
            number=request.data['contract']['number'],
            is_credit_limit_enabled=request.data['contract']['is_credit_limit_enabled'],
            credit_amount=request.data['contract']['credit_amount']
            if request.data['contract']['credit_amount']
            else legal_entity.contract.credit_amount,
        )
        LegalEntity.objects.filter(id=pk).update(
            full_name=request.data['legal_entity']['full_name'],
            legal_address=request.data['legal_entity']['legal_address'],
            actual_address=request.data['legal_entity']['actual_address'],
            inn=request.data['legal_entity']['inn'],
            kpp=request.data['legal_entity']['kpp'],
            ogrn=request.data['legal_entity']['ogrn'],
        )
        return Response({
            'legal_entity': model_to_dict(legal_entity),
            'profile': model_to_dict(legal_entity.partner_user),
            'contract': model_to_dict(legal_entity.contract),
            'account': model_to_dict(legal_entity.account),
        })

    def partial_update(self, request, pk=None):
        pass

    def destroy(self, request, pk=None):
        pass


class LoginView(viewsets.ViewSet):
    def list(self, request):
        pass

    def create(self, request):
        profile = Profile.objects.filter(email=request.data['email'], password=request.data['password']).first()
        return Response({'uuid': profile.uuid})

    def retrieve(self, request, pk=None):
        pass

    def update(self, request, pk=None):
        pass

    def partial_update(self, request, pk=None):
        pass

    def destroy(self, request, pk=None):
        pass


class DownloadAct(viewsets.ViewSet):
    def list(self, request):
        current_user = Profile.objects.filter(uuid=request.headers['Authorization']).first()
        if not current_user:
            return
        return Response({'base64_data': create_pdf_act()})

    def create(self, request):
        pass

    def retrieve(self, request, pk=None):
        pass

    def update(self, request, pk=None):
        pass

    def partial_update(self, request, pk=None):
        pass

    def destroy(self, request, pk=None):
        pass


class DownloadInvoice(viewsets.ViewSet):
    def list(self, request):
        current_user = Profile.objects.filter(uuid=request.headers['Authorization']).first()
        if not current_user:
            return
        return Response({'base64_data': create_pdf_invoice()})

    def create(self, request):
        pass

    def retrieve(self, request, pk=None):
        current_user = Profile.objects.filter(uuid=request.headers['Authorization']).first()
        if not current_user:
            return
        return Response({'base64_data': create_pdf_invoice()})

    def update(self, request, pk=None):
        pass

    def partial_update(self, request, pk=None):
        pass

    def destroy(self, request, pk=None):
        pass


class DownloadTicket(viewsets.ViewSet):
    def list(self, request):
        current_user = Profile.objects.filter(uuid=request.headers['Authorization']).first()
        if not current_user:
            return
        return Response({'base64_data': create_pdf_ticket()})

    def create(self, request):
        pass

    def retrieve(self, request, pk=None):
        pass

    def update(self, request, pk=None):
        pass

    def partial_update(self, request, pk=None):
        pass

    def destroy(self, request, pk=None):
        pass


class DownloadQR(viewsets.ViewSet):
    def list(self, request):
        current_user = Profile.objects.filter(uuid=request.headers['Authorization']).first()
        if not current_user:
            return
        return Response({'base64_data': create_image_qr()})

    def create(self, request):
        pass

    def retrieve(self, request, pk=None):
        pass

    def update(self, request, pk=None):
        pass

    def partial_update(self, request, pk=None):
        pass

    def destroy(self, request, pk=None):
        pass


class ParseIndividualXLSXTable(viewsets.ViewSet):

    def list(self, request):
        pass

    def create(self, request):
        RATES = {
            'standart': 1,
            'round_trip_standart': 2,
            'business': 3,
            'round_trip_business': 4,
        }
        PRICES = {
            1: 500,
            2: 900,
            3: 1500,
            4: 2500,
        }
        DOCUMENT_TYPE = {
            'Паспорт': 1,
            'Иной документ': 2,
        }
        current_user = Profile.objects.filter(uuid=request.headers['Authorization']).first()
        if not current_user:
            return
        total_amount = 0
        file = request.FILES['file']
        order_created = False
        db_order = None
        for order in parse_xlsx(file):
            document = Document.objects.get_or_create(
                number=order.document_number,
                type=DOCUMENT_TYPE[order.document_type]
            )
            if not order_created:
                db_order = Order.objects.create(
                    travel_date='2020-01-01',
                    amount=PRICES[RATES[request.data['rate']]],
                    rate=RATES[request.data['rate']],
                    consumer=LegalEntity.objects.filter(partner_user=current_user).first()
                )
                order_created = True
            Ticket.objects.create(
                order=db_order
            )
            Individual.objects.get_or_create(
                first_name=order.first_name,
                last_name=order.last_name,
                middle_name=order.middle_name,
                document=document[0],
                order=db_order,
            )
            total_amount += PRICES[RATES[request.data['rate']]]
        db_order.consumer.contract.credit_amount = db_order.consumer.contract.credit_amount - total_amount
        db_order.consumer.contract.save()
        return Response({'total_amount': total_amount})

    def retrieve(self, request, pk=None):
        pass

    def update(self, request, pk=None):
        pass

    def partial_update(self, request, pk=None):
        pass

    def destroy(self, request, pk=None):
        pass


class CreateOrder(viewsets.ViewSet):

    def list(self, request):
        pass

    def create(self, request):
        RATES = {
            'standart': 1,
            'round_trip_standart': 2,
            'business': 3,
            'round_trip_business': 4,
        }
        PAYMENT_TYPE = {
            'credit': 1,
            'through_accounts': 2,
            'online_pay': 3,
            'business_card': 4,
        }
        PRICES = {
            1: 500,
            2: 900,
            3: 1500,
            4: 2500,
        }
        DOCUMENT_TYPE = {
            'PASSPORT': 1,
            'OTHER': 2,
        }
        current_user = Profile.objects.filter(uuid=request.headers['Authorization']).first()
        if not current_user:
            return
        total_amount = 0
        db_order = Order.objects.create(
            travel_date=request.data['date'],
            amount=PRICES[RATES[request.data['rate']]],
            rate=RATES[request.data['rate']],
            consumer=LegalEntity.objects.filter(partner_user=current_user).first(),
            payment_type=PAYMENT_TYPE[request.data['pay_type']]
        )
        for passenger in request.data['passengers']:
            document = Document.objects.get_or_create(
                number=passenger['doc_id'],
                type=DOCUMENT_TYPE[passenger['doc_type']]
            )

            Ticket.objects.create(
                order=db_order
            )
            Individual.objects.get_or_create(
                first_name=passenger['first_name'],
                last_name=passenger['last_name'],
                middle_name=passenger['middle_name'],
                document=document[0],
                order=db_order,
            )
            total_amount += db_order.amount

        if PAYMENT_TYPE[request.data['pay_type']] == 1:
            db_order.consumer.contract.credit_amount = db_order.consumer.contract.credit_amount - total_amount
            db_order.consumer.contract.save()

        send_email_confirmation(
            from_address='aeroexpresstest@yandex.ru',
            to=current_user.email,
            legal_entity=LegalEntity.objects.filter(partner_user=current_user).first(),

        )

        return Response({
            'total_amount': total_amount,
            'credit_limit': db_order.consumer.contract.credit_amount
        })

    def retrieve(self, request, pk=None):
        current_user = Profile.objects.filter(uuid=request.headers['Authorization']).first()
        if not current_user:
            return
        current_order = Order.objects.filter(id=pk).first()

        passengers = []
        for passenger in Individual.objects.filter(order=current_order):
            passengers.append(passenger)

        tickets = []
        for ticket in Ticket.objects.filter(order=current_order):
            tickets.append(ticket)

        return Response({
            'tickets': [model_to_dict(tic) for tic in tickets],
            'passengers': [model_to_dict(pas) for pas in passengers],
            'current_order': model_to_dict(current_order),
        })

    def update(self, request, pk=None):
        pass

    def partial_update(self, request, pk=None):
        pass

    def destroy(self, request, pk=None):
        pass


class GetCreditLimit(viewsets.ViewSet):

    def list(self, request):
        current_user = Profile.objects.filter(uuid=request.headers['Authorization']).first()
        if not current_user:
            return

        return Response({'limit': LegalEntity.objects.filter(partner_user=current_user).first().contract.credit_amount})


class GetOrders(viewsets.ViewSet):

    def list(self, request):
        RATES = {
            1: 'standart',
            2: 'round_trip_standart',
            3: 'business',
            4: 'round_trip_business'
        }
        PAYMENT_TYPE = {
            1: 'credit',
            2: 'through_accounts',
            3: 'online_pay',
            4: 'business_card',
        }
        PRICES = {
            1: 500,
            2: 900,
            3: 1500,
            4: 2500,
        }
        PRICES = {
            1: 500,
            2: 900,
            3: 1500,
            4: 2500,
        }
        STATUS = {
            1: 'awaiting_payment',
            2: 'paid'
        }

        current_user = Profile.objects.filter(uuid=request.headers['Authorization']).first()
        if not current_user:
            return

        tickets = Ticket.objects.filter(order__consumer__partner_user=current_user).order_by('order').values('order').annotate(counter=Count('order')).order_by('-order')
        result = []
        for ticket in tickets:
            order = Order.objects.filter(id=ticket['order']).first()
            result.append({
                'order': ticket['order'],
                'ticket_count': ticket['counter'],
                'payment_type': PAYMENT_TYPE[order.payment_type],
                'travel_date': order.travel_date,
                'order_date': order.order_date,
                'order_status': STATUS[order.status],
                'rate': RATES[order.rate],
                'total_amount': ticket['counter'] * PRICES[order.rate]
            })
        return Response(result)


class GetXLSXTemplate(viewsets.ViewSet):
    def list(self, request):
        current_user = Profile.objects.filter(uuid=request.headers['Authorization']).first()
        if not current_user:
            return
        return Response({'base64_data': get_xlsx_template()})


class GetOrder(viewsets.ViewSet):

    def list(self, request):
        pass

    def create(self, request):
        pass

    def retrieve(self, request, pk=None):
        RATES = {
            1: 'standart',
            2: 'round_trip_standart',
            3: 'business',
            4: 'round_trip_business'
        }
        PAYMENT_TYPE = {
            1: 'credit',
            2: 'through_accounts',
            3: 'online_pay',
            4: 'business_card',
        }
        PRICES = {
            1: 500,
            2: 900,
            3: 1500,
            4: 2500,
        }

        current_user = Profile.objects.filter(uuid=request.headers['Authorization']).first()
        if not current_user:
            return
        order = Order.objects.filter(id=pk).first()
        tickets = Ticket.objects.filter(
            order__consumer__partner_user=current_user,
            order=order).order_by('order').values('order').annotate(counter=Count('order')).order_by('counter').first()

        individuals = Individual.objects.filter(order__consumer__partner_user=current_user, order=order)

        return Response({
            'order': tickets['order'],
            'ticket_count': tickets['counter'],
            'payment_type': PAYMENT_TYPE[order.payment_type],
            'travel_date': order.travel_date,
            'order_date': order.order_date,
            'rate': RATES[order.rate],
            'individuals': individuals.values(),
            'total_amount': tickets['counter'] * PRICES[order.rate]
        })

    def update(self, request, pk=None):
        pass

    def partial_update(self, request, pk=None):
        pass

    def destroy(self, request, pk=None):
        pass


class Statistic(viewsets.ViewSet):
    def list(self, request):
        current_user = Profile.objects.filter(uuid=request.headers['Authorization']).first()
        if not current_user:
            return
        count_order_by_date = Order.\
            objects.\
            annotate(date=TruncDate('order_date')). \
            annotate(sum=Sum('amount')). \
            values('date', 'sum'). \
            annotate(counter=Count('date')).order_by('date')

        return Response(count_order_by_date)


# MODELS


class ProfileViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Profile.objects.filter(deleted=False)
    serializer_class = ProfileSerializer


class AccountViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows banking accounts to be viewed or edited.
    """
    queryset = Account.objects.filter(deleted=False)
    serializer_class = AccountSerializer


class DocumentViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Documents to be viewed or edited.
    """
    queryset = Document.objects.filter(deleted=False)
    serializer_class = DocumentSerializer


class IndividualViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows individuals data to be viewed or edited.
    """
    queryset = Individual.objects.filter(deleted=False)
    serializer_class = IndividualSerializer


class ContractViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows individuals data to be viewed or edited.
    """
    queryset = Contract.objects.filter(deleted=False)
    serializer_class = ContractSerializer


class LegalEntityViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows legal entity data to be viewed or edited.
    """
    queryset = LegalEntity.objects.filter(deleted=False)
    serializer_class = LegalEntitySerializer


class OrderViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows legal entity data to be viewed or edited.
    """
    queryset = Order.objects.filter(deleted=False)
    serializer_class = OrderSerializer
