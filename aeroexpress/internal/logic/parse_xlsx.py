import base64
import datetime
import os
import tempfile

import pandas as pd


class Order:
    DOCUMENT_TYPE = {
        'PASSPORT': 1,
        'OTHER': 2,
    }
    first_name: str
    last_name: str
    middle_name: str
    document_type: str
    document_number: int
    pass


def parse_xlsx(file_path=None):
    result_list = []
    if not file_path:
        file_path = os.path.abspath('aeroexpress/static/xlsx_template.xlsx')
    else:
        from django.core.files.storage import default_storage
        file_path = default_storage.save(file_path.name, file_path)

    payload = pd.read_excel(file_path).to_dict()
    for row in range(pd.read_excel(file_path).shape[0]):
        order = Order()
        order.last_name = payload['Фамилия'][row]
        order.first_name = payload['Имя'][row]
        order.middle_name = payload['Отчество'][row]
        order.document_type = payload['Вид документа'][row]
        order.document_number = payload['Номер документа'][row]
        result_list.append(order)
    return result_list


def get_xlsx_template():
    with open(os.path.abspath('aeroexpress/static/xlsx_template.xlsx'), 'rb') as file:
        return base64.b64encode(file.read())