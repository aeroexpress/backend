import os
import smtplib
import email.mime.multipart
import email.mime.base
import email.mime.text
import email.encoders
from email.contentmanager import maintype, subtype
from typing import Optional


def send_email(
        from_address: Optional[str] = None,
        to: Optional[str] = None,
        title: Optional[str] = None,
        legal_entity=None
):
    """
    Send email with payload data
    :param legal_entity: Data of current legal entity
    :param to: email
    :param from_address: aeroexpress_email. NOW IT HARDCODE,
     but next logic is send this order will be from aeroexpress manager
    :param title: title
    :return: success bool
    """
    PASSWORD = 'Password!'

    if not legal_entity:
        return

    try:
        email_message = email.mime.multipart.MIMEMultipart()
        email_message['From'] = from_address
        email_message['To'] = to
        email_message['Subject'] = 'Личный кабинет Аэроэкспресс для Вас создан'
        body = f'Уважаемый (-ая) {legal_entity.partner_user.first_name}!\n\n' \
               f'Команда Аэроэкспресс рада сообщить, что мы создали для Вас личный кабинет, в котором вы сможете:\n' \
               f'- с легкостью делать заказы;\n' \
               f'- оплачивать заказы онлайн;\n' \
               f'- распечатывать билеты на Аэроэкспресс;\n' \
               f'- и многое другое!\n' \
               f'Ссылка для входа: ulonline.aeroexpress.ru\n\n' \
               f'Логин: {legal_entity.partner_user.email}\n' \
               f'Временный пароль: {legal_entity.partner_user.password}\n' \
               f'По любым вопросам может ответить Ваш персональный менеджер:\n\n' \
               f'{legal_entity.manager.first_name} ' \
               f'{legal_entity.manager.last_name} ' \
               f'{legal_entity.manager.phone} ' \
               f'{legal_entity.manager.email}\n\n' \
               f'C уважением,' \
               f'команда Ароэкспресс'

        email_message.attach(email.mime.text.MIMEText(body, 'plain'))

        server = smtplib.SMTP_SSL('smtp.yandex.ru:465')
        # server.starttls()
        server.login(from_address, PASSWORD)
        server.send_message(email_message)
        server.quit()

    except Exception:
        pass


def send_email_confirmation(
        from_address: Optional[str] = None,
        to: Optional[str] = None,
        title: Optional[str] = None,
        legal_entity=None
):
    """
    Send email with payload data
    :param legal_entity: Data of current legal entity
    :param to: email
    :param from_address: aeroexpress_email. NOW IT HARDCODE,
     but next logic is send this order will be from aeroexpress manager
    :param title: title
    :return: success bool
    """
    PASSWORD = 'Password!'

    if not legal_entity:
        return

    try:
        email_message = email.mime.multipart.MIMEMultipart()
        email_message['From'] = from_address
        email_message['To'] = to
        email_message['Subject'] = 'Билет на Аэроэкспресс'
        body = f'Уважаемый (-ая) {legal_entity.partner_user.first_name}!\n\n' \
               f'Ваш билет на аэроэкспресс во вложенном файле.\n' \
               f'C уважением,' \
               f'команда Ароэкспресс'
        with open(os.path.abspath('aeroexpress/static/ticket.pdf'), 'rb') as fp:
            file = email.mime.base.MIMEBase(maintype, subtype)
            file.set_payload(fp.read())
        email.encoders.encode_base64(file)
        file.add_header('Content-Disposition', 'attachment', filename='ticket.pdf')
        email_message.attach(file)
        email_message.attach(email.mime.text.MIMEText(body, 'plain'))

        server = smtplib.SMTP_SSL('smtp.yandex.ru:465')
        # server.starttls()
        server.login(from_address, PASSWORD)
        server.send_message(email_message)
        server.quit()

    except Exception:
        pass
