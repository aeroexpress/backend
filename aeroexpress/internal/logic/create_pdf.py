import base64
import tempfile

from fpdf import FPDF

import os


def _main_creation(payload):
    """
    Это должен быть большой метод для генерации ПДФ
    файлов с таблицами и прочей красотой, не уверен, что в рамках хакатона
    это можно успеть реализовать. Пока отдаем статику, дальше будет видно.
    :param payload: Instance from DB
    :return: base64 string
    """
    data = [
        [data] for data in payload.objects.filter(id=payload.id)
    ]

    pdf = FPDF()
    pdf.add_font('FreeSans', '', 'static/FreeSans.ttf', uni=True)
    pdf.set_font('FreeSans')
    pdf.add_page()

    col_width = pdf.w / 4.5
    row_height = pdf.font_size
    for row in data:
        for item in row:
            pdf.cell(col_width, row_height * 1,
                     txt=item, border=1)
        pdf.ln(row_height * 1)

    temp_pdf = tempfile.NamedTemporaryFile(suffix='.pdf')
    pdf.output(temp_pdf.name)
    with open(temp_pdf.name, 'rb') as file:
        return base64.b64encode(file.read())


def create_pdf_act(payload=None, mock=True):
    if mock and not payload:
        with open(os.path.abspath('aeroexpress/static/act.pdf'), 'rb') as file:
            return base64.b64encode(file.read())
    return _main_creation(payload)


def create_pdf_ticket(payload=None, mock=True):
    if mock and not payload:
        with open(os.path.abspath('aeroexpress/static/ticket.pdf'), 'rb') as file:
            return base64.b64encode(file.read())
    return _main_creation(payload)


def create_pdf_invoice(payload=None, mock=True):
    if mock and not payload:
        with open(os.path.abspath('aeroexpress/static/invoice.pdf'), 'rb') as file:
            return base64.b64encode(file.read())
    return _main_creation(payload)


def create_image_qr(payload=None, mock=True):
    with open(os.path.abspath('aeroexpress/static/qr.png'), 'rb') as file:
        return base64.b64encode(file.read())
