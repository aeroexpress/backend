from django.urls import path, include
from rest_framework import routers

from . import views

router = routers.DefaultRouter()
# model_view
router.register(r'profiles', views.ProfileViewSet)
router.register(r'accounts', views.AccountViewSet)
router.register(r'documents', views.DocumentViewSet)
router.register(r'individuals', views.IndividualViewSet)
router.register(r'contracts', views.ContractViewSet)
router.register(r'legal_entities', views.LegalEntityViewSet)
# router.register(r'orders', views.OrderViewSet)
#logic
router.register(r'login', views.LoginView, basename='login')
router.register(r'user_data', views.UserDataView, basename='user_data')
router.register(r'create_legal_entity', views.CreateLegalEntityView, basename='create_legal_entity')
router.register(r'download_act', views.DownloadAct, basename='download_act')
router.register(r'download_invoice', views.DownloadInvoice, basename='download_invoice')
router.register(r'download_ticket', views.DownloadTicket, basename='download_ticket')
router.register(r'download_qr', views.DownloadQR, basename='download_qr')
router.register(r'update_xlsx', views.ParseIndividualXLSXTable, basename='update_xlsx')
router.register(r'get_xlsx', views.GetXLSXTemplate, basename='get_xlsx')
router.register(r'create_order', views.CreateOrder, basename='create_order')
router.register(r'get_credit_limit', views.GetCreditLimit, basename='get_credit_limit')
router.register(r'orders', views.GetOrders, basename='orders')
router.register(r'order_data', views.GetOrder, basename='order_data')
router.register(r'statistic', views.Statistic, basename='statistic')


urlpatterns = [
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
