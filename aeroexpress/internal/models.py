from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
import uuid
from django.contrib.postgres.fields import ArrayField
from django.utils.timezone import now


class Profile(models.Model):
    """
    Employees of AEROEXPRESS
    """
    USER_ROLE = {
        'MANAGER': 'role_manager',
        'ACCOUNT': 'role_account',
        'USER': 'role_user'
    }

    first_name = models.TextField(max_length=255)
    last_name = models.TextField(max_length=255)
    middle_name = models.TextField(max_length=255)
    phone = models.TextField(max_length=255)
    email = models.TextField(max_length=255, unique=True)
    password = models.TextField(max_length=255)
    uuid = models.TextField(max_length=36, blank=True, null=True, default=str(uuid.uuid4()))
    role = models.TextField(blank=True, default=USER_ROLE['MANAGER'])
    ###
    deleted = models.BooleanField(default=False)
    deleted_date = models.DateTimeField(blank=True, null=True)

    def delete(self, using=None, keep_parents=False):
        self.deleted = True
        self.deleted_date = now()
        self.save()


class Account(models.Model):
    """
    Banking account
    """
    number = models.TextField(max_length=255)
    correspondent_account = models.TextField(max_length=255)
    bik = models.TextField(max_length=255)
    bank = models.TextField(max_length=255)
    ###
    deleted = models.BooleanField(default=False)
    deleted_date = models.DateTimeField(blank=True, null=True)

    def delete(self, using=None, keep_parents=False):
        self.deleted = True
        self.deleted_date = now()
        self.save()


class Document(models.Model):
    """
    Document data
    """
    DOCUMENT_TYPE = {
        'PASSPORT': 1,
        'OTHER': 2,
    }

    number = models.TextField(max_length=255, blank=True, null=True, unique=True)
    type = models.IntegerField(blank=True, default=DOCUMENT_TYPE['PASSPORT'])
    ###
    deleted = models.BooleanField(default=False)
    deleted_date = models.DateTimeField(blank=True, null=True)

    def delete(self, using=None, keep_parents=False):
        self.deleted = True
        self.deleted_date = now()
        self.save()


class Contract(models.Model):
    """
    Contract data
    """
    start_date = models.DateField()
    end_date = models.DateField()
    number = models.TextField(max_length=255)
    is_credit_limit_enabled = models.BooleanField(default=False)
    credit_amount = models.FloatField(default=0)
    ###
    deleted = models.BooleanField(default=False)
    deleted_date = models.DateTimeField(blank=True, null=True)

    def delete(self, using=None, keep_parents=False):
        self.deleted = True
        self.deleted_date = now()
        self.save()


class LegalEntity(models.Model):
    """
    Legal entity. Main partners
    """
    full_name = models.TextField(max_length=255)
    legal_address = models.TextField(max_length=255)
    actual_address = models.TextField(max_length=255)
    inn = models.TextField(max_length=12)
    kpp = models.TextField(max_length=12)
    ogrn = models.TextField(max_length=13)
    account = models.ForeignKey(Account, on_delete=models.PROTECT)
    partner_user = models.ForeignKey(Profile, on_delete=models.PROTECT)
    contract = models.ForeignKey(Contract, on_delete=models.PROTECT)
    manager = models.ForeignKey(Profile, on_delete=models.PROTECT, related_name='manager')
    ###
    deleted = models.BooleanField(default=False)
    deleted_date = models.DateTimeField(blank=True, null=True)

    def delete(self, using=None, keep_parents=False):
        self.deleted = True
        self.deleted_date = now()
        self.save()


class Order(models.Model):
    RATES = {
        'standart': 1,
        'round_trip_standart': 2,
        'business': 3,
        'round_trip_business': 4,
    }
    PAYMENT_TYPE = {
        'credit': 1,
        'through_accounts': 2,
        'online_pay': 3,
        'business_card': 4,
    }
    STATUS = {
        'awaiting_payment': 1,
        'paid': 2,
    }
    order_date = models.DateTimeField(default=now)
    travel_date = models.DateField()
    amount = models.FloatField()
    rate = models.IntegerField(default=RATES['standart'])
    consumer = models.ForeignKey(LegalEntity, on_delete=models.PROTECT, blank=True)
    payment_type = models.IntegerField(blank=True, default=PAYMENT_TYPE['credit'])
    status = models.IntegerField(blank=True, default=STATUS['awaiting_payment'])
    ###
    deleted = models.BooleanField(default=False)
    deleted_date = models.DateTimeField(blank=True, null=True)

    def delete(self, using=None, keep_parents=False):
        self.deleted = True
        self.deleted_date = now()
        self.save()


class Individual(models.Model):
    """
    Individual clients from legal entities
    """
    first_name = models.TextField(max_length=255)
    last_name = models.TextField(max_length=255)
    middle_name = models.TextField(max_length=255, blank=True, null=True)
    document = models.ForeignKey(Document, on_delete=models.PROTECT, blank=True, null=True)
    order = models.ForeignKey(Order, on_delete=models.PROTECT, blank=True, null=True)
    ###
    deleted = models.BooleanField(default=False)
    deleted_date = models.DateTimeField(blank=True, null=True)

    def delete(self, using=None, keep_parents=False):
        self.deleted = True
        self.deleted_date = now()
        self.save()


class Ticket(models.Model):
    """
    Ticket. Main partners
    """
    order = models.ForeignKey(Order, on_delete=models.PROTECT)
    ###
    deleted = models.BooleanField(default=False)
    deleted_date = models.DateTimeField(blank=True, null=True)

    def delete(self, using=None, keep_parents=False):
        self.deleted = True
        self.deleted_date = now()
        self.save()


class Payments(models.Model):
    """
    Payments. It will be using with sync to 1C
    """
    order = models.ForeignKey(Order, on_delete=models.PROTECT)
    amount = models.FloatField(default=0)
    ###
    deleted = models.BooleanField(default=False)
    deleted_date = models.DateTimeField(blank=True, null=True)

    def delete(self, using=None, keep_parents=False):
        self.deleted = True
        self.deleted_date = now()
        self.save()
