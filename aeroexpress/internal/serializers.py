import uuid

from rest_framework import serializers

from .models import Profile, Account, Document, Individual, Contract, LegalEntity, Order


class ShortProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = ['id', 'first_name', 'last_name', 'middle_name', 'phone', 'email', 'role']

    def create(self, validated_data):
        validated_data['uuid'] = str(uuid.uuid4())
        return Profile.objects.create(**validated_data)


class ProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = '__all__'

    def create(self, validated_data):
        validated_data['uuid'] = str(uuid.uuid4())
        return Profile.objects.create(**validated_data)


class AccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = '__all__'


class DocumentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Document
        fields = '__all__'


class IndividualSerializer(serializers.ModelSerializer):
    class Meta:
        model = Individual
        fields = '__all__'


class ContractSerializer(serializers.ModelSerializer):
    class Meta:
        model = Contract
        fields = '__all__'


class LegalEntitySerializer(serializers.ModelSerializer):
    account = AccountSerializer(many=False, read_only=True)
    partner_user = ShortProfileSerializer(many=False, read_only=True)
    contract = ContractSerializer(many=False, read_only=True)
    manager = ShortProfileSerializer(many=False, read_only=True)

    class Meta:
        model = LegalEntity
        fields = '__all__'


class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = '__all__'
